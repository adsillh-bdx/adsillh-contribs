mdfiles=contribs.md $(shell find . \( -name "*.md" ! -name contribs.md \) | sort -r)

contribs.html: head.html contribs0.html tail.html
	cat $^ > $@

contribs0.html: *.md
	pandoc -f markdown -t html -o $@ ${mdfiles}

upload: contribs.html
	scp $< labri:ENSEIGNEMENT/adsillh/

clean:
	rm -f contribs.html
