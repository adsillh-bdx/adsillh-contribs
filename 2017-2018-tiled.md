* 2017-2018: Tiled
    - [Add 'New Tileset...' button when no tileset is opened](https://github.com/bjorn/tiled/pull/1789)
    - [Open file button added when no file opened](https://github.com/bjorn/tiled/pull/1818)
