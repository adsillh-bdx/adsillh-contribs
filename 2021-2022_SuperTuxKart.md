* 2021-2022: SuperTuxKart

  * Accept emails format like e.email ([Issue](https://github.com/supertuxkart/stk-code/issues/4718), [Pull](https://github.com/supertuxkart/stk-code/pull/4721))
  * More precise error message for online username ([Issue](https://github.com/supertuxkart/stk-code/issues/3599), [Pull](https://github.com/supertuxkart/stk-code/pull/4720))
  * Left side upgrade - ghost replay difficulties (V2) ([Issue](https://github.com/supertuxkart/stk-code/issues/3849), [Pull](https://github.com/supertuxkart/stk-code/pull/4707))
  * Ghost replay difficulties (V1) ([Issue](https://github.com/supertuxkart/stk-code/issues/3849), [Pull](https://github.com/supertuxkart/stk-code/pull/4692))
  * Irrlicht error indent ([Pull](https://github.com/supertuxkart/stk-code/pull/4676))
  * Correction of the implementation.txt file ([Pull](https://github.com/supertuxkart/stk-code/pull/4655))
