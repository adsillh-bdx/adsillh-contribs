* 2016-2017: Octoprint
    - [OctoPrint FreeMobile-notifier](https://plugins.octoprint.org/plugins/freemobilenotifier/)
    - [add feature: cost per weight](https://github.com/jasiek/OctoPrint-Cost/commit/d82353b6cfc2b984a8f686ca4fc54727282df396)
